# Todos or action items

- make the result formal in the language of expected utility theory (von neumann)

   - We are uncertain whether the agent is vNM-rational

   - Further consider the following two hypotheses:
      - h_1 : The agent is not vNM-rational
      - h_2 : The agent is vNM-rational, but has a different utility function (u \neq u*)

   *Theorem 1*
   Without knowing the domain of u, it is impossible to distinguish h_1 and h_2.

   Major update, transition to broader research.
   "Theorem 1" is not really a theorem. it's just an obvious thing. [in Wikipedia page on Von Neumann–Morgenstern utility theorem, within the applicability to economics section]




   - complete the proof 
   - Let u be any utility function having domain D
   - Let u^* be an estimation 

- write motivation section

- write analysis section

- write further work section

# Post Outline

## Title

Mismatched Ontologies May Cause Illusions of Irrational Behavior

## Disclaimers/Epstemic Status

I am unsure of the correctness of this post and relatively unfamiliar with the underlying literature.

I welcome feedback; I want to improve!

## Motivation

grocery store analogy with three agents



## Result

Two agents cannot confirm strategy domination without either the same ontology or a mapping between their ontologies.

Given a sequence of actions performed by an agent, it is impossible to distinguish irrational behavior from rational behavior without knowing the ontology of the agent that made the decision.



## Proof



## Analysis


## Further work

\documentclass[12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{rotating}
\usepackage{listings}
\usepackage{hyperref}
\usepackage[letterpaper, portrait, margin=1in]{geometry}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric,arrows,fit,matrix,positioning}
\graphicspath{ {images/} }
\pagestyle{fancy}
\setlength{\headheight}{16pt}

%Setup tikz node styles
\tikzset
{
    treenode/.style = {circle, draw=black, align=center, minimum size=1cm},
    n-subtree/.style  = {isosceles triangle, draw=black, align=center, minimum width=4.8cm, shape border rotate=90, anchor=north},
    half-subtree/.style = {isosceles triangle, draw=black, align=center, minimum width=2.4cm, shape border rotate=90, anchor=north},
    every node/.style = {shape=rectangle, rounded corners,
    draw, align=center,
    top color=white, bottom color=gray!20},
    sibling distance=10em
}

%better circled numbers
%https://tex.stackexchange.com/questions/7032/good-way-to-make-textcircled-numbers
\newcommand*\circled[1]{\tikz[baseline=(char.base)]{
            \node[shape=circle,draw,inner sep=2pt] (char) {#1};}}

%Noindent environment for assignment numbers

\newenvironment{notdent}
    {\setlength\parindent{0pt}}

%Bind title, author, etc to additional variables for use beyond /maketitle
%https://tex.stackexchange.com/questions/27710/how-can-i-use-author-date-and-title-after-maketitle
\title{Naive Set Theory Sec 03}
\author{Jeyan Burns-Oorjitham}

\newcommand{\Lastname}{Burns-Oorjitham}
\newcommand{\Website}{https://jeyoor.com}
\makeatletter
%Define some macros
\let\d\displaystyle
\def\limn{\d\lim_{n \to \infty}}
\def\lf{\lfloor}
\def\rf{\rfloor}
\def\lc{\lceil}
\def\rc{\rceil}
\def\eset{\varnothing}
\let\Title\@title
\let\Author\@author
\makeatother

\fancyhf{}
\fancyhead[L]{\Title}
\fancyhead[R]{\Lastname~\thepage}

\begin{document}
\thispagestyle{plain}


\begin{notdent}

\Title\\

\Author\\

\Website\\

\end{notdent}

\section{Existence and The Empty Set}

For now, we assume that there exists a set. Later, we will have more advanced assumptions of existence.

If a set exists, then we can show as a consequence that there exists a set with no elements. We can find this empty set by applying the Axiom of Specification with a sentence that excludes all the elements within the set. A sentence that is always false (such as $x \neq x$) will exclude all elements within the set.

The empty set is denoted $\emptyset$.

\subsection{Empty Set as a Subset}

The empty set is a subset of every set.

$\forall A [\emptyset \subset A]$

At first, this vacuous truth may seem strange or counterintuitive. However, we can illuminate this fact with a bit of logical argument.

Consider what would be required to prove this statement false. By the definition of $\subset$, we would have to find an element within $\emptyset$ which does not exist in $A$ for some set $A$. However, $\emptyset$ has no elements. Thus, the statement can never be shown to be false. Thus, the statement is true.

\section{The Axiom of Pairing}

For any two sets $B$ and $C$, there exists a set $D$ to which both $B$ and $C$ belong.

$\forall B, C [ \exists D [ B \in D \land C \in D ] ]$

\subsection{Discussion and Terminology}

This axiom provides a powerful tool for constructing new sets. It can be repeatedly applied to construct larger and larger sets from multiple elements.

It is important to note that, by itself, the Axiom of Pairing does not limit its contents to \textbf{only} the elements listed. In the specific language of the theorem above, $D$ may contain elements other than $B$ and $C$.

However, the Axiom of Extension could be applied to reduce $D$ to only $B$ and $C$ as follows:

Let $E$ be the elements $f$ in $D$ for which $f = B$ or $f = C$.

$E = \{f \in D : f = B \lor f = C\}$

The generality of set theoretic language allows us to relabel the sets above without changing meaning as long as we do not modify any of the criteria.

In other words,

$\forall G, H [ \exists I [ G \in I \land H \in I] ]$ 

and 

$J = \{ i \in I : i = G \lor i = H \}$

are equivalent statements to the ones earlier in this section.

By the Axiom of Extension, $J$ is unique. (Any other set that met the criteria used to specify $J$ would, in fact, be $J$ itself.)

A set specified in this way via a combination of the Axiom of Pairing and the Axiom of Extension is called an unordered pair.

An unordered pair containing sets $K$ and $L$ is often denoted $\{K, L\}$.

\subsection{Second Look at Notation} \label{second_look}

We can use slightly different notation to restate some of the previous statements to emphasize their importance.

Let $M(n)$ be the sentence $n = o \lor n = p$ where $o$ and $p$ are both sets.

Then, the axiom of pairing states:

There will always exist some set $N$ such that $[ n \in N \iff M(n) \textrm{ is true} ]$

Let $Y$ represent this statement.

%ASIDE 2019-06-02 07:27 : I think there might be a mistake in the text on page 9 with the (*) and (**) statements. I think (*) is miswritten, but  I could easily be wrong. I have written (*) as I believe it should be written above.

%RETHINKING Afterward 2019-06-02 16:24 Maybe this was correct? should I look up other sources later?

The axiom of specification, applied to set $Q$ states

There exists a set $R$ such that $[r \in R \iff (r \in Q \land  M(r) \textrm{ is true})]$

Let $Z$ represent this statement.

$Z$ is equivalent to the Axiom of Specification with $M(r)$ as the sentence.

$Y$ is equivalent to the Axiom of Pairing for $o$ and $p$.

Thus, $Y$ can be viewed as a pseudo special case of $Z$.

The Axiom of Pairing alone does not specify uniqueness. However,  if we are able to use the Axiom of Pairing to show that a set containing $o$ and $p$ exists, we can then apply the Axiom of Specification to create a unique set that only contains $o$ and $p$.

This pattern of showing that a set exists containing all elements having a certain property, and then showing that there is a unique subset only those elements having the given property, will be repeated several times in the sections ahead.

\section{Singletons}

Let $S$ be any set.

By the Axiom of Pairing, we can form the unordered pair $\{S, S\}$. Since it contains only one element, such an unordered pair is often written simply as $\{S\}$ and referred to as a singleton.

A singleton can likewise be uniquely defined as a set that has $S$ as its only element.

Per the discussion above, $\emptyset$ and $\{ emptyset \}$ are different sets. The first set has no elements.  The second set has one element, namely $\emptyset$.

The statement $B \in C$ is the same as $\{B\} \subset C$

\section{Repeated Applications of Pairing}

\subsection{Motivation} \label{motivation}

The Axiom of Pairing implies that any two sets $D$ and $E$ are both members of some other set $F = \{D, E\}$.

It also implies that every individual set $G$ belongs to a set $\{G\}$.

From these basic implications, larger and larger sets can be constructed.

For example, consider these sets: $\emptyset, \{ \emptyset \}, \{ \{ \emptyset \} \}, \{ \{ \{ \emptyset \} \} \}$

From these sets, we may construct unordered pairs such as $\{ \emptyset, \{ \emptyset \} \}$

Each of the preceding singletons or pairs constructed could be combined into still larger pairs or singletons, and so on.


\subsection{Exercise}

Are all of the sets constructed in \ref{motivation} unique? 

\subsection{Solution}

Yes, all of the sets constructed in this way are unique.

Here is a proof provided by \href{https://github.com/gblikas/set-theory-solutions-manual/}{gblikas via GitHub}

Let $H$ and $I$ be any different sets formed in the manner described in \ref{motivation}.

By the Axiom of Specification, $H$ is the only set which contains exactly the elements in $H$. Likewise, $I$ is the only set which contains exactly the elements in $I$.

We will prove that $H \neq I$ by contradiction. 

Assume $H = I$. 

\textbf{Statement 1:} Since $H = I$, for all $h \in H$, $h \in I$ and for all $i \in I$, $i \in H$.

The sets formed in \ref{Motivation} are either unordered pairs or singletons.

\begin{itemize}
\item If $H$ and $I$ are singletons, $H = \{h\}$ and $I = \{i\}$. By Statement 1 and the Axiom of Extension, $h = i$. Since $h = i$, $H$ and $I$ must have been formed in the same way. This contradicts the fact that $H$ and $I$ were formed differently.

\item If $H$ and $I$ are unordered pairs, $H = \{ h, h' \}$ and $I = \{ i, i' \}$. By Statement 1 and the Axiom of Extension, either $[h = i \land h' = i']$ or $[h = i' \land h' = i]$
   \begin{itemize}
   \item If $[h = i \land h' = i']$, then $H$ and $I$ must have been formed in the same manner (because their respective elements are the same). This contradicts the fact that $H$ and $I$ were formed differently.
   \item If $[h = i' \land h' = i]$, then $H$ and $I$ must have been formed in the same manner (because their respective elements are the same. This contradicts the fact that $H$ and $I$ were formed differently.
   \end{itemize}

In every case, the assumption that $H = I$ leads to a contradiction. Therefore, $H \neq I$.
\end{itemize}


\section{Third Look at Notation}

There are several ways to denote some of the sets we have seen so far.

Recall statement $Y$ from \ref{second_look} which was written

$[ n \in N \iff [ n = o \lor n = p ]  ]$

We could equivalently describe $N$ like this 

$\{ n : n = o \lor n = p \}$

We could similarly describe the set $N$ as $\{o, p \}$. This more compact representation will be used where possible.

If $J(q)$ is a sentence that produces a valid set of $q$s, then we can denote that set as $\{ q : J(q) \}$.

We can even describe an arbitrary set $K$ as $\{ l : l \in K \}$

If we want to restrict $K$ to only items that meet some criteria $M(l)$, we can shorten $\{ l : l \in K \land M(l) \}$ down to $\{ l \in K : M(l) \}$

Additionally, $ \{ x : x \neq x \} = \emptyset$ and $\{ x : x = a \} = \{ a \}$

As proven toward the end of Section 2, if $R(s)$ is $(s = s)$, then $\{ s : R(s) \}$ is not a set (there is no universe).

Simlarly, if $T(s)$ is $(s \notin s)$, then $\{ s : T(s) \}$ is not a set.

Although the structures described by $T$ and $S$ are not sets and thus will not be discussed further in these notes, it is possible to create a theory that addresses them. 

Most theories that include objects such as those described by $T$ and $S$ refer to them as classes. For more information on classes, see a textbook or course on axiomatic set theory such as \textit{Discovering Modern Set Theory} by Winfried Just and Martin Weese.

\end{document}
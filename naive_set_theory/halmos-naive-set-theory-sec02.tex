\documentclass[12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{rotating}
\usepackage{listings}
\usepackage{hyperref}
\usepackage[letterpaper, portrait, margin=1in]{geometry}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric,arrows,fit,matrix,positioning}
\graphicspath{ {images/} }
\pagestyle{fancy}
\setlength{\headheight}{16pt}

%Setup tikz node styles
\tikzset
{
    treenode/.style = {circle, draw=black, align=center, minimum size=1cm},
    n-subtree/.style  = {isosceles triangle, draw=black, align=center, minimum width=4.8cm, shape border rotate=90, anchor=north},
    half-subtree/.style = {isosceles triangle, draw=black, align=center, minimum width=2.4cm, shape border rotate=90, anchor=north},
    every node/.style = {shape=rectangle, rounded corners,
    draw, align=center,
    top color=white, bottom color=gray!20},
    sibling distance=10em
}

%better circled numbers
%https://tex.stackexchange.com/questions/7032/good-way-to-make-textcircled-numbers
\newcommand*\circled[1]{\tikz[baseline=(char.base)]{
            \node[shape=circle,draw,inner sep=2pt] (char) {#1};}}

%Noindent environment for assignment numbers

\newenvironment{notdent}
    {\setlength\parindent{0pt}}

%Bind title, author, etc to additional variables for use beyond /maketitle
%https://tex.stackexchange.com/questions/27710/how-can-i-use-author-date-and-title-after-maketitle
\title{Naive Set Theory Sec 02}
\author{Jeyan Burns-Oorjitham}

\newcommand{\Lastname}{Burns-Oorjitham}
\newcommand{\Website}{https://jeyoor.com}
\makeatletter
%Define some macros
\let\d\displaystyle
\def\limn{\d\lim_{n \to \infty}}
\def\lf{\lfloor}
\def\rf{\rfloor}
\def\lc{\lceil}
\def\rc{\rceil}
\def\eset{\varnothing}
\let\Title\@title
\let\Author\@author
\makeatother

\fancyhf{}
\fancyhead[L]{\Title}
\fancyhead[R]{\Lastname~\thepage}

\begin{document}
\thispagestyle{plain}


\begin{notdent}

\Title\\

\Author\\

\Website\\

\end{notdent}

\section{Intuition}

Intuitively, we can describe a set by specifying the properties of all items in the set.

Let $A$ be the set of all birds.

Then $B = \{ x \in A : x \textrm{ is a chicken} \}$ is a specific subset of birds.

$C = \{ x \in A : x \textrm{ is not a chicken} \}$ is a different subset of birds, disjoint from $B$.

Note that $D = \{x \in A: x \textrm{ is a single chicken named Stevie with red feathers living in Oshkosh, WI} \}$ is a set containing only one item, namely one specific chicken.

Also note that $D$ is not the same thing as Stevie. A set containing a given object is not the same thing as the object itself.

\section{Sentences}

To more precisely identify sets, we should specify exactly how to write sentences that describe a set.

The two most basic types sentences are belonging:

$x \in A$

and equality

$A = B$.

All other sentences are formed from combinations of the \textit{atomic} sentences above.

Logical symbols (connectives and quantifiers) can be used to form more complex sentences by combining atomic sentences.

Here is a reproduction of the list of logical symbols from the notes on Section 1.

\begin{itemize}

\item $p \land q$ ($p$ and $q$)
\item $p \lor q$ ($p$ or $q$)
\item $\neg p$ (not $p$)
\item $ p \rightarrow q $ ($p$ implies $q$)
\item $ p \iff q$ ($p$ if and only if $q$)
\item $\forall x (p)$ (for all $x$, $p$ is true)
\item $\exists x (p)$ (there exists an $x$ for which $p$ is true)

\end{itemize}

The first five symbols are sometimes called connectives because they connect sentences (represented by $p$ and $q$).

The last two symbols are often called quantifiers. They allow us to specify how many elements of a set satisfy a given sentence.

Taken together, these symbols can represent statements in first-order logic (also called predicate logic).

\section{Axiom of Specification}

For every set $A$ and sentence $S(x)$ there is a corresponding set $B$ whose elements are those elements $x \in A$ for which $S(x)$ is true.

By the Axiom of Extension, $B$ is unique.

The set $B$ is often written as $B = \{ x \in A : S(x) \}$

\section{Hints at the Axiom of Regularity}

\subsection{Discussion}

As he did toward the end of Section 1, Halmos again hints toward the Axiom of Regularity at the end of Section 2.

Halmos drops this hint by proving that for any set $A$ it is possible to construct a subset $B$ that is not a member of $A$.

This construction shows that no set contains all other sets (which is a corollary of the Axiom of Regularity).

\subsection{Proof}

To begin the proof, let the sentence $S(x) = \neg (x \in x)$

Elsewhere, we will write $x \notin x$ instead of $\neg (x \in x)$, but the intended meaning is the same.

For any set $A$, let $B = \{ x \in A : S(x) \} = \{ x \in A : x \notin x \}$

We will prove that $B \notin A$ by contradiction. If you are unfamiliar with proof by contradiction, I recommend an introductory course in either mathematical proofs or discrete mathematics. Coursera offers one such course \href{https://www.coursera.org/learn/mathematical-thinking}{here}.

Assume $B \in A$.

Either $B \in B$ (Case 1) or $B \notin B$ (Case 2).

\subsubsection{Case 1}

$B \in A$ and $B \in B$. However, the definition of $B$ implies that for every element $y$ in $B$, $y \notin y$. Because $B \in B$, $B$ could be substituted for $y$ above. Thus, $B \in B$  contradicts the definition of $B$. This case leads to a contradiction.

\subsubsection{Case 2}

$B \in A$ and $B \notin B$. However, the definition of $B$ implies that if $z$ is an element of $A$ and $z \notin z$, then $z \in B$. Because $B \in A$, $B$ could be substituted for $z$ above. Thus, $B \notin B$ contradicts the definition of $B$.


The assumption always leads to a contradiction. Therefore, the assumption is false. $\square$

\end{document}
